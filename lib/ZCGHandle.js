const {JSHandle} = require('./JSHandle');

function createZCGHandle(context, remoteObject) {
    const frame = context.frame();
    if (frame) {
        const frameManager = frame._frameManager;
        const handle = new ZCGHandle(context, context._client, remoteObject, frameManager.page(), frameManager);        
        return new Proxy(handle, {
            get(target, prop, reciever) {
                // todo: разобраться откуда эти then приползают
                if (prop in target || prop === "then") {
                    return Reflect.get(target, prop, reciever);
                } else {                    
                    return target.executionContext().evaluate((obj, prop) => {
                        let value;
                        if (prop in obj) {
                            value = obj[prop];
                        }
                        if (typeof value === "function") {
                            return Promise.reject("can't handle function");
                        }
                        if (typeof value === "object") {
                            return Promise.reject("can't handle objects");
                        }
                        return Promise.resolve(value);
                    }, target, prop);
                }
            }
        });
        // return handle;
    }
    throw Error("Something went wrong?");
}

class ZCGHandle extends JSHandle {
    constructor(context, client, remoteObject, page, frameManager) {
        super(context, client, remoteObject);

        this._client = client;
        this._remoteObject = remoteObject;
        this._page = page;
        this._frameManager = frameManager;
        this._disposed = false;
    }

    async _clickablePoint() {
        // const result = await this.executionContext().evaluate(() => Promise.resolve(8 * 7));
        // console.log(result); // prints "56"
        const point = await this.executionContext().evaluate(obj => {
            // debugger;
            const bounds = obj.getBounds();
            const x = bounds.x + bounds.width * 0.5;
            const y = bounds.y + bounds.height * 0.5;
            console.warn(x, y);
            return Promise.resolve({ x, y });
        }, this);

        // console.log(point);
        return point;
    }

    async click(options) {
        const {x, y} = await this._clickablePoint();
        await this._page.mouse.click(x, y, options);
    }

    async type(text) {
        await this.click();
        // todo: podumat'
        await this._page.waitFor(1000);
        await this._page.keyboard.type(text);
    }
}

module.exports = { ZCGHandle, createZCGHandle };