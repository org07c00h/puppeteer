const puppeteer = require('../index');

(async() => {
  const browser = await puppeteer.launch({
      args:[ '--disable-web-security'/*, '--auto-open-devtools-for-tabs'*/ ],
      headless: false
    });
  const page = await browser.newPage();
  await page.setViewport({ width: 800, height: 600});
  await page.goto('http://localhost:8080/poker-8.17/media/pokerdommobile/html5/client/debug.html?url=https://pkd-live-ng4j5.net/&width=800&height=600&dpiPx=2&debugRatio=0.5&mobileScreenDebug=true', {waitUntil: 'networkidle0'});
  // await page.waitFor(2000);
  const inputAcc = await page.zcgWaitForSelector('inputAcc', { interactive: true });
  await inputAcc.type('pocger');
  const inputPass = await page.zcgWaitForSelector('inputPass');
  await inputPass.type('');
  const buttonLogin = await page.zcgWaitForSelector('buttonLogin');
  await buttonLogin.click();
  await page.zcgWaitForSelector('btnMenu', { interactive: true });
  await page.screenshot({path: 'example.png'});
  await browser.close();
})();
